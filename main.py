tam = 0

class Nodo:
  def _init_(self, valor = None, siguiente = None):
    self.valor = valor 
    self.siguiente = siguiente

  def Actuazar_alias (self, valor, poscion):
    indice = 1
    temp = self.cabeza 
    print("\n")
    while temp != None:
      if indice == posicion:
        temp.valor = valor 
        temp = temp.siguiente
        indice = indice + 1
      else:
        temp = temp.siguiente
        indice = indice + 1

  def Actuazar_siguiente (self, poscion, nodo):
    indice = 1
    temp = self.cabeza 
    print("\n")
    while temp != None:
      if indice == posicion:
        temp.siguiente = nodo 
        temp = temp.siguiente
        indice = indice + 1
      else:
        temp = temp.siguiente
        indice = indice + 1
    

class lista_enlazada:

  def _init_(self):
    self.cabeza = None

  def agregar_inicio (self, valor):
    global tam 
    tam = tam + 1
    temp = Nodo(valor)
    temp.siguiente = self.cabeza
    self.cabeza = temp
    del temp
    
  def agregar_fin (self, valor):
    global tam
    tam = tam + 1
    temp_n = Nodo(valor)
    temp = self.cabeza
    while temp.siguiente != None:
      temp = temp.siguiente
    temp.siguiente = temp_n

  def imprimir_contenido(self):
    temp = self.cabeza 
    print("\n")
    if self.cabeza == None:
      print("No hay elementos")
    while temp != None:
      print(temp.valor , end=' ')
      temp = temp.siguiente
    print("\n")

  def tamano(self):
    global tam
    print(tam)

  def indice(self, posicion):
    indice = 1
    temp = self.cabeza 
    print("\n")
    while temp != None:
      if indice == posicion:
        print(temp.valor , end=' ')
        temp = temp.siguiente
        indice = indice + 1
      else:
        temp = temp.siguiente
        indice = indice + 1
    print("\n")

  def limpiar (self):
    global tam
    tam = 0
    while self.cabeza.siguiente  != None:
      temp = self.cabeza
      self.cabeza = self.cabeza.siguiente 
      del temp
    del self.cabeza
    self.cabeza = None

  def revertir (self):
    tempp = None
    tempi = self.cabeza
    while tempi != None:
      temps = tempi.siguiente
      tempi.siguiente = tempp
      tempp = tempi
      tempi = temps
    self.cabeza = tempp

miLista = lista_enlazada()
miLista.agregar_inicio(5)
miLista.agregar_inicio(8)
miLista.agregar_inicio(4)
miLista.agregar_inicio(2)
miLista.agregar_inicio(1)
miLista.agregar_inicio(3)
miLista.agregar_inicio(9)
miLista.imprimir_contenido()
miLista.tamano()
miLista.indice(7)
miLista.agregar_fin(9)
miLista.imprimir_contenido()
miLista.tamano()
miLista.revertir()
miLista.imprimir_contenido()
miLista.limpiar()
miLista.agregar_inicio(1)
miLista.agregar_inicio(1)
miLista.agregar_inicio(1)
miLista.imprimir_contenido()